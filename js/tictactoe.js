/*
 * Tic Tac Toe
 *
 * A TicTacToe game written with HTML/CSS/JavaScript.
 *
 * @author: Stefan Georgiev
 */

// Variables
var boardArray = [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0]
]; // Array for the board

var winScore = 2; // Points per win
var drawScore = 1; // Points on draw
var lossScore = -1; // Points per loss

var lastTimeStartPlayer; // The player that started the previous round
var turn; // Current player turn
var score; // Saves the score
var roundWinner; // The round winner

var currentPlayerText = document.getElementById('turn'); // Span that shows the current player
var restartRoundButton = document.getElementById('restartRound'); // The restart game button
var restartGameButton = document.getElementById('restartGame'); // The restart game button
var resultText = document.getElementById('result'); // Span with the current Score

// Functions
function init() {
    restartRoundButton.addEventListener('click', newGame);
    restartGameButton.addEventListener('click', freshGame);

    score = {
        'X': 0,
        'O': 0
    };

    lastTimeStartPlayer = 'X';
    newGame();
}

function onBoxClick(e) {
    var numb = e.target.id.substring(e.target.id.length - 1, e.target.id.length);
    var row = Math.floor(numb / 3);
    var col = numb - (row * 3);

    if (isValid(row, col)) {
        e.target.classList.remove('empty');
        e.target.classList.add(turn.toLowerCase() + '-mark');

        boardArray[row][col] = turn === 'X' ? 1 : 2;

        var winner = checkWin();
        if (winner) {
            currentPlayerText.innerText = 'Player ' + roundWinner + ' has won this round!';
            addScore();
        } else if (winner == null) {
            currentPlayerText.innerText = 'Game Over! It\'s draw.';
            addScore();
        } else {
            turn = turn === 'X' ? 'O' : 'X';
            currentPlayerText.innerText = 'It\'s Player ' + turn + ' turn.';
        }
    }
}

function newGame() {
    roundWinner = '?';
    turn = lastTimeStartPlayer;

    var board = document.getElementById('board');

    while (board.firstChild) {
        board.removeChild(board.firstChild);
    }

    for (var i = 0; i < 9; i++) {
        var box = document.createElement('div');
        box.id = "box-" + i;
        box.classList.add('empty');
        box.addEventListener('click', onBoxClick);

        board.appendChild(box);
    }

    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            boardArray[i][j] = 0;
        }
    }

    currentPlayerText.innerText = 'It\'s Player ' + turn + ' turn.';
}

function freshGame() {
    if (confirm("Are you sure you want to reset the score?")) {
        score = {
            'X': 0,
            'O': 0
        };

        resultText.innerText = '[' + score['X'] + '] : [' + score['O'] + ']';

        lastTimeStartPlayer = 'X';
        newGame();
    }
}

function addScore() {
    if (roundWinner === '?') { // On draw game
        score['X'] += drawScore;
        score['O'] += drawScore;
        lastTimeStartPlayer = lastTimeStartPlayer === 'X' ? 'O' : 'X';
    } else {
        var looser = roundWinner === 'X' ? 'O' : 'X';
        score[roundWinner] += winScore;
        score[looser] += lossScore;
        lastTimeStartPlayer = turn;
    }

    resultText.innerText = '[' + score['X'] + '] : [' + score['O'] + ']';
}

function isValid(row, col) {
    if (boardArray[row][col] == 0 && roundWinner === '?') {
        return true;
    }

    return false;
}

function checkWin() {
    // check horizontal
    for (var i = 0; i < 3; i++) {
        if (boardArray[i][0] !== 0 &&
            boardArray[i][0] === boardArray[i][1] &&
            boardArray[i][0] === boardArray[i][2]) {
            roundWinner = turn;
            return true;
        }
    }

    // check vertical
    for (var j = 0; j < 3; j++) {
        if (boardArray[0][j] !== 0 &&
            boardArray[0][j] === boardArray[1][j] &&
            boardArray[0][j] === boardArray[2][j]) {
            roundWinner = turn;
            return true;
        }
    }

    // check diagonal right
    if (boardArray[0][0] !== 0 &&
        boardArray[0][0] === boardArray[1][1] &&
        boardArray[0][0] === boardArray[2][2]) {
        roundWinner = turn;
        return true;
    }

    // check diagonal left
    if (boardArray[2][0] !== 0 &&
        boardArray[2][0] === boardArray[1][1] &&
        boardArray[2][0] === boardArray[0][2]) {
        roundWinner = turn;
        return true;
    }

    // check for empty boxes
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            if (boardArray[i][j] === 0) {
                return false;
            }
        }
    }

    return null;
}

init();
